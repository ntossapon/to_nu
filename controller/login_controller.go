package controller

import (
	"bitbucket.org/ntossapo/to_nu/model"
	"bitbucket.org/ntossapo/to_nu/repository"
	"bitbucket.org/ntossapo/to_nu/service"
	"bitbucket.org/ntossapo/to_nu/util"
	"github.com/futurenda/google-auth-id-token-verifier"
	"html/template"
	"log"
	"net/http"
	"time"
)

type LoginController struct {
	userService service.UserServiceable
	repository.ProfileRepository
	sessionService service.SessionServiceable
}

func NewLoginController(userService service.UserServiceable, profileRepo repository.ProfileRepository, sessionService service.SessionServiceable) (LoginController, error) {
	return LoginController{
		userService:       userService,
		ProfileRepository: profileRepo,
		sessionService:sessionService,
	}, nil
}

/*
	REGION PAGE
 */
func (ctrl LoginController) SignInPage(w http.ResponseWriter, r *http.Request) {
	tmpl, err := template.ParseFiles("./template/signin.html")
	if err != nil {
		panic(err)
	}
	tmpl.Execute(w, nil)
}

func (ctrl LoginController) SignUpPage(w http.ResponseWriter, r *http.Request) {
	tmpl, err := template.ParseFiles("./template/signup.html")
	if err != nil {
		panic(err)
	}
	tmpl.Execute(w, nil)
}

/*
	REGION API
*/
//TODO use google use flag in database
func (ctrl LoginController) GoogleAuth(w http.ResponseWriter, r *http.Request) {
	token := r.FormValue("token")
	email := r.FormValue("email")
	fullname := r.FormValue("fullname")
	googleId := r.FormValue("googleId")

	v := googleAuthIDTokenVerifier.Verifier{}
	//TODO load aud from config
	aud := "691605833097-kja5e3fmpnrp8uunsh70ot5lj562pbs9.apps.googleusercontent.com"
	err := v.VerifyIDToken(token, []string{
		aud,
	})
	if err != nil {
		ResponseJson(http.StatusUnauthorized, w, err)
		return
	}

	exists, err := ctrl.userService.IsExistsUser(email)
	var user model.User
	redirection := "/profile"
	if !exists {
		user, err = ctrl.userService.RegisterWithGoogle(email, googleId)
		if err != nil {
			log.Println(err)
			ResponseJson(http.StatusUnauthorized, w, err)
			return
		}

		_, err = ctrl.ProfileRepository.Create(model.Profile{
			UserId:   user.Id,
			FullName: fullname,
		})
		if err != nil {
			log.Println(err)
			ResponseJson(http.StatusUnauthorized, w, err)
			return
		}
		redirection = "/profile/edit"
	}

	user, err = ctrl.userService.Login(email, "")
	if err != nil {
		ResponseJson(http.StatusUnauthorized, w, "email was registered by someone")
		return
	}

	ctrl.setSessionCookie(w, r, user)
	ResponseJson(http.StatusOK, w, redirection)
}

func (ctrl LoginController) SignIn(w http.ResponseWriter, r *http.Request) {
	email, hashedPassword := ctrl.getLoginFormData(r)
	user, err := ctrl.userService.Login(email, hashedPassword)
	if err != nil {
		switch err.Error() {
		case service.USER_DOES_NOT_EXISTS:
			log.Println(err)
			Goto503(w, r)
			return
		case service.USER_PASSWORD_WRONG:
			log.Println(err)
			Goto503(w, r)
			return
		default:
			Goto503(w, r)
		}
	}

	ctrl.setSessionCookie(w, r, user)
	http.Redirect(w, r, "/", http.StatusFound)
}

func (ctrl LoginController) SignUp(w http.ResponseWriter, r *http.Request) {
	email, hashedPassword := ctrl.getLoginFormData(r)
	user, err := ctrl.userService.Register(email, hashedPassword)
	if err != nil {
		switch err.Error() {
		case service.USER_ALREADY_EXISTS:
		default:
			log.Println(err)
			Goto503(w, r)
			return
		}
	}

	user, err = ctrl.userService.Login(user.Email, user.PasswordHash)
	if err != nil {
		switch err.Error() {
		case service.USER_DOES_NOT_EXISTS:
			log.Println(err)
		case service.USER_PASSWORD_WRONG:
			log.Println(err)
		default:
			log.Println(err)
			Goto503(w, r)
			return
		}
	}

	ctrl.setSessionCookie(w, r, user)
	http.Redirect(w, r, "/profile", http.StatusFound)
}

func (ctrl LoginController) LogOut(w http.ResponseWriter, r *http.Request) {
	session := GetSessionFromRequest(r)
	if session != "" {
		ctrl.sessionService.Delete(session)
	}

	cookie := http.Cookie{
		Name:   "SESSION_ID",
		MaxAge: -1,
	}
	http.SetCookie(w, &cookie)
	http.Redirect(w, r, "/signin", http.StatusFound)
}

func (ctrl LoginController) getLoginFormData(r *http.Request) (string, string) {
	return r.FormValue("email"), util.SHA512(r.FormValue("password"))
}

func (ctrl LoginController) setSessionCookie(w http.ResponseWriter, r *http.Request, user model.User) error {
	expired := time.Now().Add(15 * 24 * time.Hour)
	http.SetCookie(w, &http.Cookie{
		Name:    "SESSION_ID",
		Path:    "/",
		Value:   user.CurrentSession,
		Expires: expired,
	})

	return nil
}
