//TODO Move middle level business logic to service level.
package controller

import (
	"bitbucket.org/ntossapo/to_nu/model"
	"bitbucket.org/ntossapo/to_nu/repository"
	"bitbucket.org/ntossapo/to_nu/service"
	"bitbucket.org/ntossapo/to_nu/view_model"
	"errors"
	"github.com/gorilla/mux"
	"html/template"
	"log"
	"net/http"
	"reflect"
	"strconv"
)

type ProfileController struct {
	sessionService    service.SessionServiceable
	profileRepository repository.ProfileRepository
	userRepository repository.UserRepository
}

func NewProfileController(sessionService service.SessionServiceable, profileRepository repository.ProfileRepository, userRepository repository.UserRepository) (ProfileController, error) {
	return ProfileController{
		sessionService:    sessionService,
		profileRepository: profileRepository,
		userRepository: userRepository,
	}, nil
}

func (ctrl ProfileController) View(w http.ResponseWriter, r *http.Request) {
	tmpl, err := template.ParseFiles("./template/profile.html")
	if err != nil {
		log.Println(err)
		Goto503(w, r)
		return
	}

	session, err := ctrl.sessionService.Find(GetSessionFromRequest(r))
	if err != nil {
		log.Println(err)
		Goto503(w, r)
		return
	}

	profile, err := ctrl.profileRepository.FindByUserId(session.User.Id)
	if err != nil {
		log.Println(err)
		Goto503(w, r)
		return
	}

	//profile is not created
	if (profile.Id == 0) {
		ctrl.goToEditProfile(w, r)
		return
	}

	vm := view_model.NewProfilePageViewMode(session.User, profile)

	tmpl.Execute(w, vm)
}

func (ctrl ProfileController) EditPage(w http.ResponseWriter, r *http.Request) {

	tmpl, err := template.ParseFiles("./template/profile.edit.html")
	if err != nil {
		log.Println(err)
		Goto503(w, r)
		return
	}

	session, err := ctrl.sessionService.Find(GetSessionFromRequest(r))
	if err != nil {
		log.Println(err)
		Goto503(w, r)
		return
	}

	profile, err := ctrl.profileRepository.FindByUserId(session.User.Id)
	if err != nil {
		log.Println(err)
		Goto503(w, r)
		return
	}

	// It is not created yet. First time sign up.
	if profile.Id == 0 {
		profile = model.Profile{}
	}

	vm := view_model.NewProfilePageViewMode(session.User, profile)

	tmpl.Execute(w, vm)
}

/*
	API
*/

/*
	GET /api/profile
*/
func (ctrl ProfileController) Get(w http.ResponseWriter, r *http.Request) {
	sessionId := GetSessionFromRequest(r)
	session, err := ctrl.sessionService.Find(sessionId)
	if err != nil {
		ResponseJson(http.StatusInternalServerError, w, err)
		return
	}

	profile, err := ctrl.profileRepository.FindByUserId(session.User.Id)
	if err != nil {
		ResponseJson(http.StatusInternalServerError, w, err)
		return
	}

	if profile.UserId == 0 {
		ResponseJson(http.StatusNotFound, w, errors.New("user not found"))
		return
	}

	ResponseJson(http.StatusOK, w, profile)
}

/*
	POST /api/profile
*/
func (ctrl ProfileController) Create(w http.ResponseWriter, r *http.Request) {
	profile, err := ctrl.createProfileModelFromRequest(r)
	if err != nil {
		ResponseJson(http.StatusInternalServerError, w, err)
		return
	}

	_, err = ctrl.profileRepository.Create(profile)
	if err != nil {
		ResponseJson(http.StatusInternalServerError, w, err)
		return
	}

	profile, err = ctrl.profileRepository.FindByUserId(profile.UserId)
	if err != nil {
		ResponseJson(http.StatusInternalServerError, w, err)
		return
	}

	ResponseJson(http.StatusOK, w, profile)
}

/*
	PUT /api/profile
*/
func (ctrl ProfileController) Edit(w http.ResponseWriter, r *http.Request) {
	updatingProfile, err := ctrl.createProfileModelFromRequest(r)
	if err != nil {
		log.Println(err)
		ResponseJson(http.StatusInternalServerError, w, err)
		return
	}

	profile, err := ctrl.profileRepository.FindByUserId(updatingProfile.UserId)
	if err != nil {
		log.Println(err)
		ResponseJson(http.StatusInternalServerError, w, err)
		return
	}
	updatingProfile.Id = profile.Id
	if !reflect.DeepEqual(updatingProfile, profile) {
		err := ctrl.profileRepository.Update(updatingProfile)
		if err != nil {
			log.Println(err)
			ResponseJson(http.StatusInternalServerError, w, err)
			return
		}
	}

	//Does it need to change email ?
	sessionId := GetSessionFromRequest(r)
	session, err := ctrl.sessionService.Find(sessionId)
	if err != nil {
		log.Println(err)
		ResponseJson(http.StatusInternalServerError, w, err)
		return
	}
	log.Println(session.User.Email, r.FormValue("email"))
	if session.User.Email != r.FormValue("email") {
		err = ctrl.updateEmail(w, r)
		if err != nil {
			log.Println(err)
			ResponseJson(http.StatusInternalServerError, w, err)
			return
		}

		err = ctrl.sessionService.Delete(sessionId)
		if err != nil {
			log.Println(err)
			ResponseJson(http.StatusInternalServerError, w, err)
			return
		}

		ctrl.LogOut(w, r)
		return
	}

	ResponseJson(http.StatusOK, w, updatingProfile)
}

func (ctrl ProfileController) Delete(w http.ResponseWriter, r *http.Request) {
	v := mux.Vars(r)
	profileIdStr := v["id"]

	profileId, err := strconv.ParseInt(profileIdStr, 10, 32)
	if err != nil {
		log.Println(err)
		ResponseJson(http.StatusInternalServerError, w, err)
		return
	}

	/*
		Checking permission
	 */
	session, err := ctrl.sessionService.Find(GetSessionFromRequest(r))
	if err != nil {
		log.Println(err)
		ResponseJson(http.StatusInternalServerError, w, err)
		return
	}

	userProfile, err := ctrl.profileRepository.FindByUserId(session.User.Id)
	if err != nil {
		log.Println(err)
		ResponseJson(http.StatusInternalServerError, w, err)
		return
	}

	if userProfile.UserId == 0 {
		log.Println("user has no profile yet")
		ResponseJson(http.StatusInternalServerError, w, errors.New("user has no profile yet"))
		return
	}

	if userProfile.Id != int(profileId) {
		log.Println("permission denied")
		ResponseJson(http.StatusInternalServerError, w, errors.New("permission denied"))
		return
	}

	err = ctrl.profileRepository.Delete(int(profileId))
	if err != nil {
		log.Println(err)
		ResponseJson(http.StatusInternalServerError, w, err)
		return
	}

	ResponseJson(http.StatusOK, w, "")
}

/*
	HELPER
 */

func (ctrl ProfileController) LogOut(w http.ResponseWriter, r *http.Request) {
	cookie := http.Cookie{
		Name:   "SESSION_ID",
		MaxAge: -1,
	}
	http.SetCookie(w, &cookie)
	ResponseJson(http.StatusOK, w, "kill session")
}

func (ctrl ProfileController) updateEmail (w http.ResponseWriter, r *http.Request) error{
	email := r.FormValue("email")
	sessionId := GetSessionFromRequest(r)
	session, err := ctrl.sessionService.Find(sessionId)
	if err != nil {
		return err
	}

	_, err = ctrl.userRepository.UpdateEmail(session.User.Email, email)
	if err != nil {
		return err
	}

	return nil
}

func (ctrl ProfileController) goToEditProfile(w http.ResponseWriter, r *http.Request) {
	http.Redirect(w, r, "/profile/edit", http.StatusFound)
}

func (ctrl ProfileController) createProfileModelFromRequest(r *http.Request) (model.Profile, error) {
	sessionId := GetSessionFromRequest(r)
	session, err := ctrl.sessionService.Find(sessionId)
	if err != nil {
		return model.Profile{}, err
	}

	fullName := r.FormValue("full_name")
	address := r.FormValue("address")
	lat, _ := strconv.ParseFloat(r.FormValue("lat"), 64)
	long, _ := strconv.ParseFloat(r.FormValue("long"), 64)
	tel := r.FormValue("telephone")

	return model.Profile{
		UserId:    session.User.Id,
		FullName:  fullName,
		Address:   address,
		Lat:       lat,
		Long:      long,
		Telephone: tel,
	}, nil
}
