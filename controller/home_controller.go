package controller

import (
	"net/http"
)

type HomeController struct {
}

func NewHomeController () (HomeController, error) {
	return HomeController{
	}, nil
}

func (hc HomeController) responseError(res http.ResponseWriter, req *http.Request){

}

func (hc HomeController) Home (w http.ResponseWriter, r *http.Request){
	//for _, c := range r.Cookies() {
	//	http.SetCookie(w, c)
	//}

	http.Redirect(w, r, "/profile", http.StatusFound)
}