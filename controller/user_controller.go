package controller

import (
	"bitbucket.org/ntossapo/to_nu/repository"
	"bitbucket.org/ntossapo/to_nu/service"
	"bitbucket.org/ntossapo/to_nu/util"
	"html/template"
	"log"
	"net/http"
	"time"
)

type UserController struct {
	emailService          service.EmailService
	userRepository        repository.UserRepository
	newPasswordRepository repository.NewPasswordRepository
}

func NewUserController(
	userRepository repository.UserRepository,
	emailService service.EmailService,
	newPasswordRepository repository.NewPasswordRepository,
) UserController {
	return UserController{
		emailService:          emailService,
		userRepository:        userRepository,
		newPasswordRepository: newPasswordRepository,
	}
}

func (ctrl UserController) RequestResetPassword(w http.ResponseWriter, r *http.Request) {
	email := r.FormValue("email")
	user, err := ctrl.userRepository.FindByEmail(email)
	if err != nil {
		log.Println(err)
		Goto503(w, r)
		return
	}

	if user.Id == 0 {
		log.Println("request reset password: user not found")
		GoToSignIn(w, r)
		return
	}

	err = ctrl.newPasswordRepository.Remove(user.Id)
	if err != nil {
		log.Println(err)
		Goto503(w, r)
		return
	}

	token := util.SHA512(time.Now().String())
	req, err := ctrl.newPasswordRepository.Create(user.Id, token)
	if err != nil {
		log.Println(err)
		GoToSignIn(w, r)
		return
	}

	ctrl.emailService.SendForgetEmail(user, req.Token)
	http.Redirect(w, r, "/signin", http.StatusFound)
}

func (ctrl UserController) NewPassword (w http.ResponseWriter, r *http.Request){
	token := r.FormValue("token")
	newPassword := r.FormValue("password")

	request, err := ctrl.newPasswordRepository.ReadByToken(token)
	if err != nil {
		log.Println(err)
		Goto503(w, r)
		return
	}

	user, err := ctrl.userRepository.Find(request.UserId)
	if err != nil {
		log.Println(err)
		Goto503(w, r)
		return
	}

	if user.Id == 0 {
		log.Println("")
		Goto503(w, r)
		return
	}

	err = ctrl.userRepository.UpdatePassword(user.Id, util.SHA512(newPassword))
	if err != nil {
		log.Println(err)
		Goto503(w, r)
		return
	}

	err = ctrl.newPasswordRepository.Remove(user.Id)
	if err != nil {
		log.Println(err)
		Goto503(w, r)
		return
	}

	GoToSignIn(w, r)
}

func (ctrl UserController) ResetPasswordPage(w http.ResponseWriter, r *http.Request) {
	tmpl, err := template.ParseFiles("./template/reset.html")
	if err != nil {
		log.Println(err)
		Goto503(w, r)
		return
	}

	tmpl.Execute(w, nil)
}
func (ctrl UserController) NewPasswordPage(w http.ResponseWriter, r *http.Request) {
	token := r.URL.Query().Get("p")
	tmpl, err := template.ParseFiles("./template/new_password.html")
	if err != nil {
		Goto503(w, r)
		return
	}

	tmpl.Execute(w, struct {
		Token string
	}{
		Token: token,
	})
}
