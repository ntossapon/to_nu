package controller

import (
	"encoding/json"
	"html/template"
	"net/http"
)

func GoToSignIn(w http.ResponseWriter, r *http.Request) {
	http.Redirect(w, r, "/signin", http.StatusFound)
}

func Goto503(w http.ResponseWriter, r *http.Request) {
	tmpl, err := template.ParseFiles("./template/503.html")
	if err != nil {
		panic(err)
	}

	tmpl.Execute(w, r)
}

func GetSessionIdFromCookie (r *http.Request) string {
	cookie, err := r.Cookie("SESSION_ID")
	if err != nil {
		return ""
	}

	return cookie.Value
}

func GetSessionIdFromHeader(r *http.Request) string {
	return r.Header.Get("X-Session-Token")
}

func GetSessionFromRequest(r *http.Request) string {
	if session := GetSessionIdFromHeader(r); session != "" {
		return session
	}else{
		return GetSessionIdFromCookie(r)
	}
}

func ResponseJson(status int, w http.ResponseWriter, value interface{}){
	switch value.(type) {
	case error:
		ResponseJson(status, w, struct {
			Error string
		}{
			Error:value.(error).Error(),
		})
	default:
		b, err := json.Marshal(value)
		if err != nil {
			ResponseJson(status, w, err)
		}

		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(status)
		w.Write(b)
	}
}
