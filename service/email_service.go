package service

import (
	"bitbucket.org/ntossapo/to_nu/lib/email"
	"bitbucket.org/ntossapo/to_nu/model"
	"fmt"
)

type forgetEmailMessage struct {
	FromName    string
	FromEmail   string
	ToName      string
	ToEmail     string
	Subject     string
	HtmlContent string
}

type EmailService interface {
	SendForgetEmail(user model.User, resetParam string)
}

type EmailServiceImplementation struct {
	emailBuffer chan forgetEmailMessage
	emailClient email.EmailClient
}

func NewEmailService(emailClient email.EmailClient) EmailService {

	instance := EmailServiceImplementation{
		emailClient: emailClient,
		emailBuffer:make(chan forgetEmailMessage, 100),
	}

	return instance
}

func (service EmailServiceImplementation) SendForgetEmail(user model.User, resetParam string) {
	fromName := "no-reply-tonu"
	fromEmail := "no-reply-tonu@tonu.grafana48.com"
	toName := "You"
	toEmail := user.Email
	subject := "Reset your password"
	content := fmt.Sprintf(`
		<body>
			<a href="http://tonu.grafana48.com/new_password?p=%s">reset password here</a>
		</body>
	`, resetParam)

	em := forgetEmailMessage{
		FromName:fromName,
		FromEmail:fromEmail,
		ToName:toName,
		ToEmail:toEmail,
		Subject:subject,
		HtmlContent:content,
	}

	go service.emailClient.Send(
		em.FromName,
		em.FromEmail,
		em.ToName,
		em.ToEmail,
		em.Subject,
		em.HtmlContent,
	)
}
