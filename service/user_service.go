package service

import (
	"bitbucket.org/ntossapo/to_nu/model"
	"bitbucket.org/ntossapo/to_nu/repository"
	"github.com/pkg/errors"
)

const (
	USER_DOES_NOT_EXISTS = "user doesn't exists"
	USER_ALREADY_EXISTS = "user already exists"
	USER_PASSWORD_WRONG = "password wrong"
)

type UserServiceable interface {
	Register (email, hashedPassword string) (model.User,  error)
	RegisterWithGoogle(email, googleUserId string) (model.User,  error)
	Login (email, passwordHash string) (model.User, error)
	IsExistsUser(email string) (bool, error)
}

type UserService struct {
	userRepository repository.UserRepository
	sessionService SessionServiceable
}

func NewUserService (userRepository repository.UserRepository, sessionService SessionServiceable) UserServiceable {
	return UserService{
		userRepository:userRepository,
		sessionService:sessionService,
	}
}

func (service UserService) RegisterWithGoogle(email, googleUserId string) (model.User,  error){
	user, err := service.Register(email, "")
	if err != nil {
		return model.User{}, err
	}

	err = service.userRepository.UpdateGoogleUserId(user.Id, googleUserId)
	if err != nil {
		return model.User{}, err
	}

	return service.userRepository.FindByEmail(user.Email)
}

func (service UserService) Register (email, hashedPassword string) (model.User,  error){
	user, err := service.userRepository.FindByEmail(email)
	if err != nil {
		return model.User{}, err
	}

	if user.Id != 0 {
		return user, errors.New(USER_ALREADY_EXISTS)
	}

	_, err = service.userRepository.Create(email, hashedPassword)
	if err != nil {
		return model.User{}, err
	}

	return service.userRepository.FindByEmail(email)
}

func (service UserService) Login (email, passwordHash string) (model.User, error) {
	user, err := service.userRepository.FindByEmail(email)
	if err != nil {
		return model.User{}, err
	}

	if user.Id == 0 {
		return model.User{}, errors.New(USER_DOES_NOT_EXISTS)
	}

	if passwordHash != user.PasswordHash {
		return model.User{}, errors.New(USER_PASSWORD_WRONG)
	}

	user, err = service.sessionService.Create(user)
	if err != nil {
		return model.User{}, err
	}


	return user, nil
}

func (service UserService) IsExistsUser(email string) (bool, error) {
	user, err := service.userRepository.FindByEmail(email)
	if err != nil {
		return false, err
	}
	if user.Id == 0 {
		return false, nil
	}
	return true, nil
}


