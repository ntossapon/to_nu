package service

import (
	"bitbucket.org/ntossapo/to_nu/model"
	"bitbucket.org/ntossapo/to_nu/repository"
	"errors"
	"testing"
)

const (
	TEST_EMAIL         = "email@email.com"
	TEST_HASH_PASSWORD = "password_hashed"
	TEST_SESSION_ID    = "session_id"
	TEST_USER_ID       = 1
)

func TestUserService_Login(t *testing.T) {
	userRepoMock := repository.NewUserRepoMock()
	sessionServiceMock := NewSessionServiceMock()

	userRepoMock.OnFindByEmail = func(email string) (model.User, error) {
		if email == TEST_EMAIL {
			return model.User{
				Email:        TEST_EMAIL,
				Id:           TEST_USER_ID,
				PasswordHash: TEST_HASH_PASSWORD,
			}, nil
		} else {
			return model.User{}, errors.New("could not find user")
		}
	}

	sessionServiceMock.OnCreate = func(user model.User) (model.User, error) {
		if user.Email == TEST_EMAIL {
			session := TEST_SESSION_ID
			user.CurrentSession = session
			return user, nil
		} else {
			return model.User{}, errors.New("could not create session")
		}
	}

	service := NewUserService(*userRepoMock, *sessionServiceMock)
	user, err := service.Login(TEST_EMAIL, TEST_HASH_PASSWORD)
	if err != nil {
		t.Error(err)
	}

	if user.Id != TEST_USER_ID {
		t.Error("expect", TEST_USER_ID, "actual", user.Id)
	}

	if user.CurrentSession != TEST_SESSION_ID {
		t.Error("expect", TEST_SESSION_ID, "actual", user.CurrentSession)
	}
}

func TestUserService_Register(t *testing.T) {
	userRepoMock := repository.NewUserRepoMock()
	created := false
	userRepoMock.OnFindByEmail = func(email string) (model.User, error) {
		if email == TEST_EMAIL {
			if created {
				return model.User{Id: TEST_USER_ID, Email: TEST_EMAIL, PasswordHash: TEST_HASH_PASSWORD}, nil
			} else {
				return model.User{}, nil
			}
		} else {
			return model.User{Id: 2, Email: "other@email.com", PasswordHash: ""}, nil
		}
	}

	userRepoMock.OnCreate = func(email, passwordHash string) (int64, error) {
		if email == TEST_EMAIL && passwordHash == TEST_HASH_PASSWORD {
			created = true
			return TEST_USER_ID, nil
		} else {
			return 0, errors.New("could not create new user")
		}
	}

	service := NewUserService(*userRepoMock, nil)
	user, err := service.Register(TEST_EMAIL, TEST_HASH_PASSWORD)
	if err != nil {
		t.Error(err)
	}

	if user.Email != TEST_EMAIL {
		t.Error("expect", TEST_EMAIL, "actual", user.Email)
	}

	if user.PasswordHash != TEST_HASH_PASSWORD {
		t.Error("expect", TEST_HASH_PASSWORD, "actual", user.PasswordHash)
	}

	if user.Id != TEST_USER_ID {
		t.Error("expect", TEST_USER_ID, "actual", user.Id)
	}
}
