package service

import (
	"errors"
	"github.com/go-redis/redis"
)

func NewRedis() (*redis.Client, error){
	client := redis.NewClient(&redis.Options{
		Addr:"localhost:6379",
		Password:"",
		DB:0,
	})

	if client == nil {
		return nil, errors.New("could not connect to redis")
	}

	return client, nil
}
