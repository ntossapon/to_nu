package service

import (
	"bitbucket.org/ntossapo/to_nu/model"
	"bitbucket.org/ntossapo/to_nu/repository"
	"testing"
)

func TestSessionService_Create(t *testing.T) {
	userRepositoryMock := repository.NewUserRepoMock()
	userRepositoryMock.OnUpdateSessionId = func(email, sessionId string) (model.User, error) {
		return model.User{Email:email, CurrentSession:sessionId}, nil
	}

	defer removeSession(t)
	service := NewSessionService(userRepositoryMock)
	user, err := service.Create(model.User{
		Email:TEST_EMAIL,
	})

	if err != nil {
		t.Error(err)
	}

	if user.CurrentSession == "" {
		t.Error("expect", "", "but", user.CurrentSession)
	}
}

func removeSession(t *testing.T){
	redis, err := NewRedis()
	if err != nil {
		t.Error(err)
	}

	if err := redis.FlushAll().Err(); err != nil {
		t.Error(err)
	}
}

