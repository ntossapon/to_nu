package service

import (
	"bitbucket.org/ntossapo/to_nu/model"
	"bitbucket.org/ntossapo/to_nu/repository"
	"bitbucket.org/ntossapo/to_nu/util"
	"encoding/json"
	"fmt"
	"time"
)

type SessionModel struct {
	SessionId string
	User model.User
}

type SessionServiceable interface {
	Create(user model.User) (model.User, error)
	Find(sessionId string) (SessionModel, error)
	Delete(sessionId string) error
}

type SessionService struct {
	userRepository	repository.UserRepository
}

func NewSessionService(userRepository repository.UserRepository) SessionServiceable {
	return SessionService{
		userRepository:userRepository,
	}
}

func (ss SessionService) Delete(sessionId string) error {
	client, err := NewRedis()
	if err != nil {
		return err
	}

	return client.Del(sessionId).Err()
}

func (ss SessionService) Create(user model.User) (model.User, error) {
	client, err := NewRedis()
	if err != nil {
		return model.User{}, err
	}

	now := time.Now()
	sessionId := util.SHA512(fmt.Sprintf("%s%s", now.String(), user.Email))
	sessionModel := SessionModel{
		SessionId: sessionId,
		User:user,
	}
	jsonSessionModel, err := json.Marshal(sessionModel)
	if err != nil {
		return model.User{}, err
	}

	err = client.Set(sessionId, string(jsonSessionModel), 0).Err()
	if err != nil {
		return model.User{}, err
	}

	user, err = ss.userRepository.UpdateSessionId(user.Email, sessionId)

	return user, err
}

func (ss SessionService) Find(sessionId string) (SessionModel, error) {
	client, err := NewRedis()
	if err != nil {
		return SessionModel{}, err
	}

	s, err := client.Get(sessionId).Result()
	if err != nil {
		return SessionModel{}, err
	}

	var sessionModel SessionModel
	err = json.Unmarshal([]byte(s), &sessionModel)
	if err != nil {
		return SessionModel{}, err
	}

	return sessionModel, nil
}
