package service

import "bitbucket.org/ntossapo/to_nu/model"

type SessionServiceMock struct {
	OnCreate func(user model.User) (model.User, error)
	OnFind   func(sessionId string) (SessionModel, error)
	OnDelete func(sessionId string) error
}

func NewSessionServiceMock() *SessionServiceMock {
	mock := SessionServiceMock{}
	return &mock
}

func (mock SessionServiceMock) Create(user model.User) (model.User, error) {
	return mock.OnCreate(user)
}

func (mock SessionServiceMock) Find(sessionId string) (SessionModel, error) {
	return mock.OnFind(sessionId)
}

func (mock SessionServiceMock) Delete(sessionId string) error {
	return mock.OnDelete(sessionId)
}
