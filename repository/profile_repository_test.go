package repository

import (
	"bitbucket.org/ntossapo/to_nu/model"
	"reflect"
	"testing"
)

const (
	TEST_PROFILE_ID        = 1
	TEST_PROFILE_USERID    = 1
	TEST_PROFILE_FULLNAME  = "john doe"
	TEST_PROFILE_ADDRESS   = "CA"
	TEST_PROFILE_ADDRLAT   = 1.0
	TEST_PROFILE_ADDRLONG  = 1.0
	TEST_PROFILE_TELEPHONE = "0000000000"
)

var basicProfile = model.Profile{
	UserId:    TEST_PROFILE_USERID,
	FullName:  TEST_PROFILE_FULLNAME,
	Address:   TEST_PROFILE_ADDRESS,
	Lat:       TEST_PROFILE_ADDRLAT,
	Long:      TEST_PROFILE_ADDRLONG,
	Telephone: TEST_PROFILE_TELEPHONE,
}

func TestProfileRepo_Create(t *testing.T) {
	repo := NewProfileRepository()
	defer repo.DeleteByUserId(TEST_PROFILE_USERID)
	id, err := repo.Create(basicProfile)

	if err != nil {
		t.Error(err)
	}

	if id == 0 {
		t.Error("expect", "not 0", "actual", id)
	}
}

func TestProfileRepo_FindByUserId(t *testing.T) {
	repo := NewProfileRepository()
	defer repo.DeleteByUserId(TEST_PROFILE_USERID)
	_, err := repo.Create(basicProfile)
	if err != nil {
		t.Error(err)
	}

	profile, err := repo.FindByUserId(basicProfile.UserId)
	if err != nil {
		t.Error(err)
	}
	//ignore id
	profile.Id = 0

	if !reflect.DeepEqual(basicProfile, profile) {
		t.Error("expect", basicProfile, "actual", profile)
	}
}

func TestProfileRepo_Update(t *testing.T) {
	repo := NewProfileRepository()
	defer repo.DeleteByUserId(TEST_PROFILE_USERID)
	id, err := repo.Create(basicProfile)
	if err != nil {
		t.Error(err)
	}

	updatedProfile := model.Profile{
		Id:        int(id),
		UserId:    TEST_PROFILE_USERID,
		FullName:  "bob",
		Address:   "new address",
		Lat:       2.0,
		Long:      2.0,
		Telephone: TEST_PROFILE_TELEPHONE,
	}

	err = repo.Update(updatedProfile)
	if err != nil {
		t.Error(err)
	}

	profile, err := repo.FindByUserId(TEST_PROFILE_USERID)
	if err != nil {
		t.Error(err)
	}

	if !reflect.DeepEqual(updatedProfile, profile) {
		t.Error("expect", updatedProfile, "actual", profile)
	}
}

func TestProfileRepo_DeleteByUserId(t *testing.T) {
	repo := NewProfileRepository()
	_, err := repo.Create(basicProfile)
	if err != nil {
		t.Error(err)
	}

	err = repo.DeleteByUserId(basicProfile.UserId)
	if err != nil {
		t.Error(err)
	}

	profile, err := repo.FindByUserId(basicProfile.UserId)
	if err != nil {
		t.Error(err)
	}

	if !reflect.DeepEqual(model.Profile{}, profile) {
		t.Error("expect", model.Profile{}, "actual", profile)
	}
}

func TestProfileRepo_Delete(t *testing.T) {
	repo := NewProfileRepository()
	id, err := repo.Create(basicProfile)
	if err != nil {
		t.Error(err)
	}

	err = repo.Delete(int(id))
	if err != nil {
		t.Error(err)
	}

	p, err := repo.Find(int(id))
	if err != nil {
		t.Error(nil)
	}

	if p.Id != 0 {
		t.Error("expect", "0", "actual", p.Id)
	}
}
