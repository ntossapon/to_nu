package repository

import "bitbucket.org/ntossapo/to_nu/model"

type UserRepoMock struct {
	OnFindByEmail        func(email string) (model.User, error)
	OnCreate             func(email, passwordHash string) (int64, error)
	OnRemove             func(email string) error
	OnUpdateSessionId    func(email, sessionId string) (model.User, error)
	OnUpdateEmail        func(old, new string) (model.User, error)
	OnUpdateGoogleUserId func(id int, userId string) error
}

func NewUserRepoMock() *UserRepoMock {
	mock := UserRepoMock{}
	return &mock
}

func (mock UserRepoMock) FindByEmail(email string) (model.User, error) {
	return mock.OnFindByEmail(email)
}

func (mock UserRepoMock) Create(email, passwordHash string) (int64, error) {
	return mock.OnCreate(email, passwordHash)
}

func (mock UserRepoMock) Remove(email string) error {
	return mock.OnRemove(email)
}

func (mock UserRepoMock) UpdateSessionId(email, sessionId string) (model.User, error) {
	return mock.OnUpdateSessionId(email, sessionId)
}

func (mock UserRepoMock) UpdateEmail(old, new string) (model.User, error) {
	return mock.OnUpdateEmail(old, new)
}

func (mock UserRepoMock) UpdateGoogleUserId(id int, userId string) error{
	return mock.OnUpdateGoogleUserId(id, userId)
}
