package repository

import "testing"

func TestNewPasswordRepo_Create(t *testing.T) {
	repo := NewNewPasswordRepository()
	v, err := repo.Create(1, "123")
	if err != nil {
		t.Error(err)
	}

	if v.Token != "123" {
		t.Error("Expect", "123", "actual", v.Token)
	}

	if v.Id == 0 {
		t.Error("expect not", "0", "actual", v.Id)
	}

	if v.UserId != 1 {
		t.Error("expect", 1, "actual", v.UserId)
	}
}
