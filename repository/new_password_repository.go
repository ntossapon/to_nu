package repository

import "bitbucket.org/ntossapo/to_nu/model"

type NewPasswordRepository interface {
	Read(userId int) (model.NewPasswordRequest, error)
	ReadByToken(token string) (model.NewPasswordRequest, error)
	Create(userId int, token string) (model.NewPasswordRequest, error)
	Remove(userId int) error
}

type NewPasswordRepo struct {
}

func NewNewPasswordRepository() NewPasswordRepository {
	return NewPasswordRepo{}
}

func (repo NewPasswordRepo) Read(userId int) (model.NewPasswordRequest, error) {
	db, err := OpenSql()
	if err != nil {
		return model.NewPasswordRequest{}, err
	}
	defer db.Close()

	row, err := db.Query(`
		SELECT id, user_id, token 
		FROM NewPassword
		WHERE user_id = ?
	`, userId)
	if err != nil {
		return model.NewPasswordRequest{}, err
	}

	if row.Next() {
		data := model.NewPasswordRequest{}
		err = row.Scan(&data.Id, &data.UserId, &data.Token)
		if err != nil {
			return model.NewPasswordRequest{}, err
		}

		return data, nil
	} else {
		return model.NewPasswordRequest{}, nil
	}
}

func (repo NewPasswordRepo) ReadByToken(token string) (model.NewPasswordRequest, error){
	db, err := OpenSql()
	if err != nil {
		return model.NewPasswordRequest{}, err
	}
	defer db.Close()

	row, err := db.Query(`
		SELECT id, user_id, token 
		FROM NewPassword
		WHERE token = ?
	`, token)

	if err != nil {
		return model.NewPasswordRequest{}, err
	}

	if row.Next() {
		data := model.NewPasswordRequest{}
		err = row.Scan(&data.Id, &data.UserId, &data.Token)
		if err != nil {
			return model.NewPasswordRequest{}, err
		}

		return data, nil
	} else {
		return model.NewPasswordRequest{}, nil
	}
}

func (repo NewPasswordRepo) Create(userId int, token string) (model.NewPasswordRequest, error) {
	db, err := OpenSql()
	if err != nil {
		return model.NewPasswordRequest{}, err
	}
	defer db.Close()

	_, err = db.Exec(`
		INSERT INTO NewPassword (user_id, token) VALUES(?, ?)
	`, userId, token)
	if err != nil {
		return model.NewPasswordRequest{}, err
	}

	return repo.Read(userId)
}

func (repo NewPasswordRepo) Remove(userId int) error {
	db, err := OpenSql()
	if err != nil {
		return err
	}
	defer db.Close()

	_, err = db.Exec(`
		DELETE FROM NewPassword WHERE user_id = ?
	`, userId)

	return err
}
