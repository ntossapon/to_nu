package repository

import (
	"bitbucket.org/ntossapo/to_nu/model"
	"database/sql"
)

type ProfileRepository interface {
	FindByUserId (userId int) (model.Profile, error)
	Find(id int) (model.Profile, error)
	Create(profile model.Profile) (int64, error)
	Update(profile model.Profile) error
	Delete(id int) error
	DeleteByUserId (userId int) error
}

type ProfileRepo struct {
}

func NewProfileRepository() ProfileRepository {
	return ProfileRepo{}
}

func (repo ProfileRepo) FindByUserId (userId int) (model.Profile, error) {
	db, err := OpenSql()
	if err != nil {
		return model.Profile{}, err
	}

	row, err := db.Query(`
		SELECT id, user_id, full_name, address, addr_lat, addr_long, telephone
		FROM Profiles
		WHERE user_id = ?
	`, userId)

	if err != nil {
		return model.Profile{}, err
	}
	var profile model.Profile
	if row.Next() {
		var addr sql.NullString
		var lat sql.NullFloat64
		var long sql.NullFloat64
		var telephone sql.NullString

		err = row.Scan(
			&profile.Id,
			&profile.UserId,
			&profile.FullName,
			&addr,
			&lat,
			&long,
			&telephone,
			)
		if err != nil {
			return model.Profile{}, err
		}

		profile.Address = addr.String
		profile.Lat = lat.Float64
		profile.Long = long.Float64
		profile.Telephone = telephone.String
	}else{
		return model.Profile{}, nil
	}

	return profile, nil
}

func (repo ProfileRepo) Find (id int) (model.Profile, error) {
	db, err := OpenSql()
	if err != nil {
		return model.Profile{}, err
	}

	row, err := db.Query(`
		SELECT id, user_id, full_name, address, addr_lat, addr_long, telephone
		FROM Profiles
		WHERE id = ?
	`, id)

	if err != nil {
		return model.Profile{}, err
	}

	var profile model.Profile
	if row.Next() {
		var addr sql.NullString
		var lat sql.NullFloat64
		var long sql.NullFloat64
		var telephone sql.NullString

		err = row.Scan(
			&profile.Id,
			&profile.UserId,
			&profile.FullName,
			&addr,
			&lat,
			&long,
			&telephone,
		)
		if err != nil {
			return model.Profile{}, err
		}

		profile.Address = addr.String
		profile.Lat = lat.Float64
		profile.Long = long.Float64
		profile.Telephone = telephone.String
	}else{
		return model.Profile{}, nil
	}

	return profile, nil
}

func (repo ProfileRepo) Create(profile model.Profile) (int64, error) {
	db, err := OpenSql()
	if err != nil {
		return 0, err
	}

	result, err := db.Exec(`
		INSERT INTO Profiles(user_id, full_name, address, addr_lat, addr_long, telephone)
		VALUES (?, ?, ?, ?, ?, ?);
	`, profile.UserId, profile.FullName, profile.Address, profile.Lat, profile.Long, profile.Telephone)

	if err != nil {
		return 0, err
	}

	return result.LastInsertId()
}

func (repo ProfileRepo) Update(profile model.Profile) error {
	db, err := OpenSql()
	if err != nil {
		return err
	}

	_, err = db.Exec(`
		UPDATE Profiles
		SET 
			full_name = ?,
			address = ?,
			addr_lat = ?,
			addr_long = ?,
			telephone = ?
		WHERE id = ?
	`,
	profile.FullName,
	profile.Address,
	profile.Lat,
	profile.Long,
	profile.Telephone,
	profile.Id)

	return err
}

func (repo ProfileRepo) DeleteByUserId (userId int) error {
	db, err := OpenSql()
	if err != nil {
		return err
	}

	_, err = db.Exec(`
		DELETE FROM Profiles
		WHERE user_id = ?
	`, userId)

	if err != nil {
		return err
	}

	return nil
}

func (repo ProfileRepo) Delete (id int) error {
	db, err := OpenSql()
	if err != nil {
		return err
	}

	_, err = db.Exec(`
		DELETE FROM Profiles
		WHERE id = ?
	`, id)

	if err != nil {
		return err
	}

	return nil
}
