package repository

import (
	"bitbucket.org/ntossapo/to_nu/model"
	"database/sql"
	_ "github.com/go-sql-driver/mysql"
)

const (
	//It's default value of int
	INVALID_USER_ID = 0
)

type UserRepository interface {
	Find(id int) (model.User, error)
	FindByEmail(email string) (model.User, error)
	Create(email, passwordHash string) (int64, error)
	Remove(email string) error
	UpdateSessionId(email, sessionId string) (model.User, error)
	UpdateEmail(old, new string) (model.User, error)
	UpdateGoogleUserId (id int, userId string) error
	UpdatePassword(id int, hashPassword string) error
}

type UserRepo struct {
}

func NewUserRepository() UserRepository {
	return UserRepo{}
}

func (repo UserRepo) Create(email, passwordHash string) (int64, error) {
	db, err := OpenSql()
	defer db.Close()
	if err != nil {
		return INVALID_USER_ID, err
	}

	result, err := db.Exec(`
		INSERT INTO Users(email, password_hash) VALUES (?, ?)
	`, email, passwordHash)

	if err != nil {
		return INVALID_USER_ID, err
	}

	return result.LastInsertId()
}

func (repo UserRepo) UpdateGoogleUserId (id int, userId string) error {
	db, err := OpenSql()
	defer db.Close()
	if err != nil {
		return err
	}

	_, err = db.Exec(`
		UPDATE Users
		SET google_user_id = ?
		WHERE id = ?
	`, userId, id)

	return err
}

func (repo UserRepo) UpdatePassword (id int, hashedPassword string) error {
	db, err := OpenSql()
	defer db.Close()
	if err != nil {
		return err
	}

	_, err = db.Exec(`
		UPDATE Users
		SET password_hash = ?
		WHERE id = ?
	`, hashedPassword, id)

	return err
}

func (repo UserRepo) Remove(email string) error {
	db, err := OpenSql()
	defer db.Close()
	if err != nil {
		return err
	}

	_, err = db.Exec(`
		DELETE FROM Users
		WHERE email = ?
	`, email)
	if err != nil {
		return err
	}

	return nil
}

func (repo UserRepo) Find(id int) (model.User, error) {
	db, err := OpenSql()
	defer db.Close()
	if err != nil {
		return model.User{}, err
	}

	row, err := db.Query(`
		SELECT id, email, password_hash, current_session FROM Users
		WHERE id = ?
	`, id)
	if err != nil {
		return model.User{}, err
	}

	user := model.User{}
	if row.Next() {
		var session sql.NullString
		err := row.Scan(&user.Id, &user.Email, &user.PasswordHash, &session)
		user.CurrentSession = session.String
		if err != nil {
			return model.User{}, err
		}
	} else {
		return model.User{}, nil
	}

	return user, nil
}

func (repo UserRepo) FindByEmail(email string) (model.User, error) {
	db, err := OpenSql()
	defer db.Close()
	if err != nil {
		return model.User{}, err
	}

	row, err := db.Query(`
		SELECT id, email, password_hash, current_session FROM Users
		WHERE email = ?
	`, email)
	if err != nil {
		return model.User{}, err
	}

	user := model.User{}
	if row.Next() {
		var session sql.NullString
		err := row.Scan(&user.Id, &user.Email, &user.PasswordHash, &session)
		user.CurrentSession = session.String
		if err != nil {
			return model.User{}, err
		}
	} else {
		return model.User{}, nil
	}

	return user, nil
}

func (repo UserRepo) UpdateSessionId(email, sessionId string) (model.User, error) {
	db, err := OpenSql()
	defer db.Close()
	if err != nil {
		return model.User{}, err
	}

	_, err = db.Exec(`
		UPDATE Users
		SET current_session = ?
		WHERE email = ?
	`, sessionId, email)

	if err != nil {
		return model.User{}, err
	}

	return repo.FindByEmail(email)
}

func (repo UserRepo) UpdateEmail(old, new string) (model.User, error){
	db, err := OpenSql()
	defer db.Close()
	if err != nil {
		return model.User{}, err
	}

	_, err = db.Exec(`
		UPDATE Users
		SET email = ?
		WHERE email = ?
	`, new, old)
	if err != nil {
		return model.User{}, err
	}

	return repo.FindByEmail(new)
}
