package repository

import (
	"fmt"
	"testing"
)

const (
	TEST_EMAIL    = "email@email.com"
	TEST_PASSWORD = "hash_message"
	TEST_EXPECT_EXISTS_EMAIL = "Error 1062: Duplicate entry 'email@email.com' for key 'email'"
	TEST_SESSIONID = "sessionId"
)

func TestUserRepo_Create(t *testing.T) {
	repo := NewUserRepository()
	defer repo.Remove(TEST_EMAIL)

	_, err := repo.Create(TEST_EMAIL, TEST_PASSWORD)
	if err != nil {
		t.Error(err)
	}

	err = repo.Remove(TEST_EMAIL)
	if err != nil {
		t.Error(err)
	}
}

func TestUserRepo_ShouldNotAllowUserExistsEmail(t *testing.T) {
	repo := NewUserRepository()
	defer repo.Remove(TEST_EMAIL)

	_, err := repo.Create(TEST_EMAIL, TEST_PASSWORD)
	if err != nil {
		t.Error(err)
	}
	_, err = repo.Create(TEST_EMAIL, TEST_PASSWORD)
	if err == nil {
		t.Error("expect not allow to create new user, actual created new user")
	}

	if err.Error() != TEST_EXPECT_EXISTS_EMAIL {
		t.Error(fmt.Sprintf("expect error message %s, actual %s", TEST_EXPECT_EXISTS_EMAIL, err.Error()))
	}
}

func TestUserRepo_FindByEmail(t *testing.T) {
	repo := NewUserRepository()
	defer repo.Remove(TEST_EMAIL)

	_, err := repo.Create(TEST_EMAIL, TEST_PASSWORD)
	if err != nil {
		t.Error(err)
	}

	user, err := repo.FindByEmail(TEST_EMAIL)
	if err != nil {
		t.Error(err)
	}

	if user.Email != TEST_EMAIL {
		t.Error("expect", TEST_EMAIL, "actual", user.Email)
	}

	if user.PasswordHash != TEST_PASSWORD {
		t.Error("expect", TEST_PASSWORD, "actual", user.PasswordHash)
	}

	if user.CurrentSession != "" {
		t.Error("expect", "", "actual", user.CurrentSession)
	}

	if user.Id == INVALID_USER_ID {
		t.Error("expect", "not 0", "actual", INVALID_USER_ID)
	}
}

func TestUserRepo_UpdateSessionId(t *testing.T) {
	repo := NewUserRepository()
	defer repo.Remove(TEST_EMAIL)

	_, err := repo.Create(TEST_EMAIL, TEST_PASSWORD)
	if err != nil {
		t.Error(err)
	}

	user, err := repo.UpdateSessionId(TEST_EMAIL, TEST_SESSIONID)
	if err != nil {
		t.Error(err)
	}

	if user.Email != TEST_EMAIL {
		t.Error("expect", TEST_EMAIL, "actual", user.Email)
	}

	if user.CurrentSession != TEST_SESSIONID {
		t.Error("expect", TEST_SESSIONID, "actual", user.CurrentSession)
	}
}