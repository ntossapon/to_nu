#!/usr/bin/env bash

docker-compose -f docker-compose.dev.yml up -d
echo "Wait for mysql start up"
sleep 20
echo "Migrate test database"
./init.sh
echo "Start unit test and database test"
go test ./...
docker-compose -f docker-compose.dev.yml down
