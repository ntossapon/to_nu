var path = require('path');
var webpack = require('webpack');

module.exports = {
    entry: {
        edit_profile: './client_side/app_edit_profile.js',
        sign_in: './client_side/app_signin.js',
        profile: './client_side/app_profile.js'
    },
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: '[name].bundle.js'
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: "babel-loader"
                }
            }
        ]
    },
    watch: true
};