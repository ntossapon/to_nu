package view_model

import "bitbucket.org/ntossapo/to_nu/model"

type ProfilePageViewModel struct {
	Profile model.Profile
	UserId	int
	Email	string
}

func NewProfilePageViewMode (user model.User, profile model.Profile) ProfilePageViewModel {
	return ProfilePageViewModel{
		Profile:profile,
		UserId:user.Id,
		Email:user.Email,
	}
}
