package util

import (
	"crypto/sha512"
	"fmt"
)

func SHA512 (s string) string {
	generator := sha512.New()
	generator.Write([]byte(s))
	b := generator.Sum(nil)
	return fmt.Sprintf("%x", b)
}