package model

type User struct {
	Id             int
	Email          string
	PasswordHash   string
	CurrentSession string
}
