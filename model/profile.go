package model

type Profile struct {
	Id	int
	UserId int
	FullName string
	Address string
	Lat float64
	Long float64
	Telephone string
}
