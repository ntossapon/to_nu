package model

type NewPasswordRequest struct {
	Id int
	UserId int
	Token string
}
