package middleware

import (
	"log"
	"net/http"
)

type PathLogger struct{}

func NewPathLogger() Middleware {
	return PathLogger{}
}

func (mw PathLogger) Handle(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		log.Println(r.Method, r.URL.Path)
		next.ServeHTTP(w, r)
	})
}
