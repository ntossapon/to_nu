package middleware

import (
	"bitbucket.org/ntossapo/to_nu/service"
	"fmt"
	"log"
	"net/http"
	"regexp"
)

type AuthMiddleware struct {
	staticReg      *regexp.Regexp
	sessionService service.SessionServiceable
	ignorePath     []string
}

func NewAuthMiddleware(SessionService service.SessionServiceable, ignorePath ...string) Middleware {
	r, _ := regexp.Compile(`\/dist\/[\w.]*`)
	return AuthMiddleware{
		staticReg:r,
		sessionService: SessionService,
		ignorePath:     ignorePath,
	}
}

func (mw AuthMiddleware) isStaticFile(path string) bool {
	return mw.staticReg.MatchString(path)
}

func (mw AuthMiddleware) Handle(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if mw.isStaticFile(r.URL.Path) {
			next.ServeHTTP(w, r)
			return
		}

		pp := fmt.Sprintf("%s %s", r.Method, r.URL.Path)

		for _, value := range mw.ignorePath {
			if value == pp {
				next.ServeHTTP(w, r)
				return
			}
		}

		sessionId := ""
		cookie, err := r.Cookie("SESSION_ID")
		if err != nil {
			sessionId = r.Header.Get("X-Session-Token")
		} else {
			sessionId = cookie.Value
		}

		_, err = mw.sessionService.Find(sessionId)
		if err != nil {
			log.Println("could not find session from redis")
			http.Redirect(w, r, "/signin", http.StatusFound)
			return
		}

		next.ServeHTTP(w, r)
	})
}
