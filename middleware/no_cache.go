package middleware

import (
	"net/http"
)

type NoCache struct{}

func NewNoCache() Middleware {
	return NoCache{}
}

func (mw NoCache) Handle(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Cache-Control", "no-store, must-revalidate, max-age=0")
		w.Header().Set("Pragma", "no-cache")
		w.Header().Set("Expires", "Sat, 01 Jan 2000 00:00:00 GMT")
		next.ServeHTTP(w, r)
	})
}
