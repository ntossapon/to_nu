package main

import (
	"bitbucket.org/ntossapo/to_nu/repository"
	"bitbucket.org/ntossapo/to_nu/service"
)

func main(){
	db, err := repository.OpenSql()
	defer db.Close()
	if err != nil {
		panic(err)
	}

	_, err = db.Exec(`
		DROP DATABASE to_nu;
	`)

	if err != nil {
		panic(err)
	}

	redis, err := service.NewRedis()
	if err != nil {
		panic(err)
	}

	err = redis.FlushAll().Err()
	if err != nil {
		panic(err)
	}
}