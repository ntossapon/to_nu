package main

import (
	"bitbucket.org/ntossapo/to_nu/repository"
	"database/sql"
)

func main() {
	s, err := sql.Open("mysql", "root:test@/")
	defer s.Close()
	if err != nil {
		panic(err)
	}
	_, err = s.Exec(`
		CREATE DATABASE IF NOT EXISTS to_nu CHARACTER SET utf8 COLLATE utf8_unicode_ci;
	`)
	if err != nil {
		panic(err)
	}

	db, err := repository.OpenSql()
	defer db.Close()
	if err != nil {
		panic(err)
	}

	_, err = db.Exec(`
		CREATE TABLE IF NOT EXISTS Users(
			id INT(12) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
			email VARCHAR(30) NOT NULL UNIQUE,
			password_hash VARCHAR (128) NOT NULL,
			google_user_id VARCHAR(300),
			current_session VARCHAR (128)
		);
	`)
	if err != nil {
		panic(err)
	}

	_, err = db.Exec(`
		CREATE TABLE IF NOT EXISTS Profiles(
			id INT(12) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
			user_id INT(12) UNSIGNED NOT NULL UNIQUE,
			full_name VARCHAR(100) NOT NULL,
			address VARCHAR(1000),
			addr_lat FLOAT,
			addr_long FLOAT,
			telephone VARCHAR (20),
			FOREIGN KEY (user_id) REFERENCES Users(id)
		);
	`)
	if err != nil {
		panic(err)
	}

	_, err = db.Exec(`
		CREATE TABLE IF NOT EXISTS NewPassword(
			id INT(12) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
			user_id INT(12) UNSIGNED NOT NULL,
			token VARCHAR (128) NOT NULL,
			FOREIGN KEY (user_id) REFERENCES Users(id)
		)
	`)
	if err != nil {
		panic(err)
	}

	_, err = db.Exec(`
		INSERT INTO Users(email, password_hash) VALUES (?, ?)	
	`, "email@test.com", "password")
	if err != nil {
		panic(err)
	}
}
