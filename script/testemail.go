package main

import (
	"bitbucket.org/ntossapo/to_nu/lib/email"
	"log"
	"os"
)

func main() {
	useClient()
	//log.Println(os.Getenv("SENDGRID_API_KEY"))
	//from := mail.NewEmail("no-reply", "no-reply@grafana48.com")
	//subject := "Example Email"
	//to := mail.NewEmail("Tossapon Nuanchuay", "ntossapo@gmail.com")
	//content := "This is content"
	//htmlContent := "<strong>Strong</strong>"
	//message := mail.NewSingleEmail(from, subject, to, content, htmlContent)
	//client := sendgrid.NewSendClient(os.Getenv("SENDGRID_API_KEY"))
	//res, err := client.Send(message)
	//if err != nil {
	//	panic(err)
	//} else {
	//	log.Println(res.StatusCode)
	//	log.Println(res.Body)
	//	log.Println(res.Headers)
	//}
}

func useClient() {
	log.Println(os.Getenv("SENDGRID_API_KEY"))
	ec := email.NewEmailClient()
	ec.Send(
		"no-reply",
		"no-reply@grafana48.com",
		"Tossapon Nuanchuay",
		"ntossapo@gmail.com",
		"Subject",
		"<h1>Hello</h1>",
	)
}
