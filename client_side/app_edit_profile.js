import API from "./api"

class EditProfileApp {
    constructor(api) {
        this.API = api;
        this.Map = {}
        document.getElementById("save").onclick = this.OnClickEdit.bind(this);
        gapi.load('auth2', function() {
            gapi.auth2.init();
        });
    }

    getElem(id) {
        return document.getElementById(id);
    }

    async OnClickEdit() {
        let email = this.getElem("email").value;
        let userId = this.getElem("user_id").value;
        let fullName = this.getElem("full_name").value;
        let address = this.getElem("address").value;
        let lat = this.Marker ? this.Marker.getPosition().lat() : 9999;
        let long = this.Marker ? this.Marker.getPosition().lng() : 9999;
        let tel = this.getElem("telephone").value;
        let needCreateNew = false;
        try {
            let existsProfile = await this.API.FindProfile(userId);
            if (existsProfile) {
                console.log("updating profile");
                let result = await this.API.Update(email, fullName, address, lat, long, tel);
                if (result === "kill session"){
                    this.KillSession();
                }

                location.replace("/profile");
            } else {
                needCreateNew = true
            }
            return;
        } catch (e) {
            if (e.message === "404") {
                console.log("creating new profile");
                needCreateNew = true;
            } else {
                console.log(e)
            }
        }

        if (needCreateNew) {
            try {
                let res = await this.API.Create(email, fullName, address, lat, long, tel);
                console.log(res);
                location.replace("/profile");
            } catch (e) {
                console.log(e);
            }
        }
    }


    InitMap(position) {
        console.log("init map in class");
        if (position.lat == 9999 || position.lng == 9999) {
            position = {lat: 0, lng: 0}
        }

        this.Map = new google.maps.Map(document.getElementById('map'), {
            center: position, zoom: 16
        });

        if (position.lat != 9999 || position.lng != 9999) {
            this.Marker = new google.maps.Marker({
                position: position,
                title: "Here",
                map: this.Map
            });

            this.Marker.addListener("click", (ev) => {
                this.Marker.setMap(null);
                this.Marker = undefined;
            })
        }

        this.Map.addListener("click", (ev) => {
            if (this.Marker) {
                this.Marker.setMap(null);
            }

            this.Marker = new google.maps.Marker({
                position: ev.latLng,
                title: "Here",
                map: this.Map
            });

            this.Marker.addListener("click", (ev) => {
                this.Marker.setMap(null);
                this.Marker = undefined;
            })
        });
    }

    KillSession() {
        gapi.auth2.getAuthInstance().signOut().then(function () {
            console.log('User signed out.');
            location.replace("/killsession");
        });
    }
}

const api = new API();
const app = new EditProfileApp(api);
window.app = app;

