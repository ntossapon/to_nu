import API from "./api"

class SignInPage {
    constructor(api) {
        this.API = api
        if (document.cookie.indexOf("SESSION_ID") > 0){
            document.location.href = "/profile"
        }
    }

    async SignInWithGoogle(googleUser) {
        let profile = googleUser.getBasicProfile();
        let email = profile.getEmail();
        let id = profile.getId()
        let fullName = profile.getName();
        let id_token = googleUser.getAuthResponse().id_token;

        console.log(id_token);

        let redirectTo = await this.API.SignInWithGoogle(id_token, email, fullName, id);

        document.location.href = redirectTo
    }
}

let api = new API();
let app = new SignInPage(api);

gapi.load('auth2', function() {
    gapi.auth2.init();
});
window.app = app;
window.onSignIn = app.SignInWithGoogle.bind(app)
