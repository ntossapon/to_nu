//TODO make a page class
class ProfilePage {
    constructor(position){
        this.DefaultPosition = position;
        console.log(this.DefaultPosition);
    }

    InitAuth(){
        console.log("load")
        gapi.load('auth2', function() {
            gapi.auth2.init();
        });
    }

    InitMap(){
        if(this.DefaultPosition.lat == 9999 || this.DefaultPosition.lng == 9999){
            document.getElementById("map").remove();
            return;
        }

        map = new google.maps.Map(document.getElementById('map'), {
            center: this.DefaultPosition, zoom: 16
        });
        map.setOptions({draggable: false});

        let marker = new google.maps.Marker({position: position, map: map});
    }

    SignOut() {
        gapi.auth2.getAuthInstance().signOut().then(function () {
            console.log('User signed out.');
            location.replace("/logout");
        });
    }
}

window.ProfilePage = ProfilePage