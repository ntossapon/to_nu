import 'whatwg-fetch'

class API {
    async FindProfile(id) {
        let res = await fetch("/api/profile", {
            credentials: 'same-origin'
        });
        if (res.status === 404) {
            let err = await res.json();
            throw new Error("404")
        }
        return await res.json()
    }

    async Create(email, fullName, address, lat, long, phone) {
        let data = new FormData();

        data.append("email", email);
        data.append("full_name", fullName);
        data.append("address", address);
        data.append("lat", lat);
        data.append("long", long);
        data.append("telephone", phone);

        let res = await fetch("/api/profile", {
            method: "POST",
            body: data,
            credentials: 'same-origin',
        });

        if (res.status != 200) {
            let err = await res.json();
            throw new Error(err);
        }

        return await res.json();
    }

    async Update(email, fullName, address, lat, long, phone) {
        let data = new FormData();

        data.append("email", email);
        data.append("full_name", fullName);
        data.append("address", address);
        data.append("lat", lat);
        data.append("long", long);
        data.append("telephone", phone);

        let res = await fetch("/api/profile", {
            method: "PUT",
            body: data,
            credentials: 'same-origin',
        })

        if (res.status != 200) {
            let err = await res.json();
            throw new Error(err);
        }

        return await res.json();
    }

    async SignInWithGoogle(token, email, fullname, id) {
        var data = new FormData();
        data.append("token", token);
        data.append("email", email);
        data.append("fullname", fullname);
        data.append("googleId", id);

        let res = await fetch(`/api/auth/google`, {
            method: "post",
            body: data,
            credentials: 'include'
        });

        let json = await res.json()

        if (res.status != 200) {
            throw new Error(err);
        }

        return json;
    }
}

export default API;