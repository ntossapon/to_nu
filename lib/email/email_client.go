package email

import (
	"github.com/sendgrid/sendgrid-go"
	"github.com/sendgrid/sendgrid-go/helpers/mail"
	"log"
	"os"
)

type EmailClient interface {
	Send(
		fromName string,
		fromEmail string,
		toName string,
		toEmail string,
		subject string,
		htmlContent string,
	)
}

type EmailClientImplementation struct {
	sendGridClient *sendgrid.Client
}

func NewEmailClient() EmailClient {
	client := sendgrid.NewSendClient(os.Getenv("SENDGRID_API_KEY"))
	return EmailClientImplementation{
		sendGridClient:client,
	}
}

func (client EmailClientImplementation) Send(
	fromName string,
	fromEmail string,
	toName string,
	toEmail string,
	subject string,
	htmlContent string,
) {
	from := mail.NewEmail(fromName, fromEmail)
	to := mail.NewEmail(toName, toEmail)
	message := mail.NewSingleEmail(from, subject, to, htmlContent, htmlContent)
	res, err := client.sendGridClient.Send(message)
	if err != nil {
		log.Println(err)
	}

	log.Println(res.Body)
}
