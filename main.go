package main

import (
	"bitbucket.org/ntossapo/to_nu/controller"
	"bitbucket.org/ntossapo/to_nu/lib/email"
	"bitbucket.org/ntossapo/to_nu/middleware"
	"bitbucket.org/ntossapo/to_nu/repository"
	"bitbucket.org/ntossapo/to_nu/service"
	"github.com/gorilla/mux"
	"log"
	"net/http"
)

func main() {
	emailClient := email.NewEmailClient()

	emailService := service.NewEmailService(emailClient)

	userRepository := repository.NewUserRepository()
	profileRepository := repository.NewProfileRepository()
	newPasswordRepository := repository.NewNewPasswordRepository()
	sessionService := service.NewSessionService(userRepository)
	userService := service.NewUserService(userRepository, sessionService)


	authMiddleware := middleware.NewAuthMiddleware(sessionService,
		"GET /signin",
					"POST /signin",
					"GET /signup",
					"POST /signup",
					"POST /api/auth/google",
					"GET /killsession",
					"GET /reset",
					"POST /reset",
					"GET /new_password",
					"POST /new_password")
	pathLogger := middleware.NewPathLogger()
	noCache := middleware.NewNoCache()

	home, err := controller.NewHomeController()
	login, err := controller.NewLoginController(userService, profileRepository, sessionService)
	profile, err := controller.NewProfileController(sessionService, profileRepository, userRepository)
	userCtrl := controller.NewUserController(userRepository, emailService, newPasswordRepository)

	if err != nil {
		log.Panic(err)
	}

	r := mux.NewRouter()

	r.Use(
		pathLogger.Handle,
		authMiddleware.Handle,
		noCache.Handle,
	)

	/*
	Web Pages
	 */
	r.HandleFunc("/", home.Home).Methods("GET")
	r.HandleFunc("/signin", login.SignInPage).Methods("GET")
	r.HandleFunc("/signup", login.SignUpPage).Methods("GET")
	r.HandleFunc("/profile", profile.View).Methods("GET")
	r.HandleFunc("/profile/edit", profile.EditPage).Methods("GET")
	r.HandleFunc("/reset", userCtrl.ResetPasswordPage).Methods("GET")
	r.HandleFunc("/new_password", userCtrl.NewPasswordPage).Methods("GET")

	/*
	API FOR WEB
	 */
	r.HandleFunc("/signin", login.SignIn).Methods("POST")
	r.HandleFunc("/signup", login.SignUp).Methods("POST")
	r.HandleFunc("/killsession", login.LogOut).Methods("GET")
	r.HandleFunc("/logout", login.LogOut).Methods("GET")
	r.HandleFunc("/reset", userCtrl.RequestResetPassword).Methods("POST")
	r.HandleFunc("/new_password", userCtrl.NewPassword).Methods("POST")
	/*
	APIs
	 */
	r.HandleFunc("/api/profile", profile.Get).Methods("GET")
	r.HandleFunc("/api/profile", profile.Create).Methods("POST")
	r.HandleFunc("/api/profile", profile.Edit).Methods("PUT")
	r.HandleFunc("/api/profile/{id}", profile.Delete).Methods("DELETE")

	r.HandleFunc("/api/auth/google", login.GoogleAuth).Methods("POST")

	/*
	Statics
	 */
	r.PathPrefix("/dist/").Handler(http.StripPrefix("/dist/", http.FileServer(http.Dir("./dist"))))

	log.Println("Starting Server @ port 80")

	http.ListenAndServe(":80", r)
}
